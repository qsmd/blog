# qsmd’s Blog

My public blog at [`www.qsmd.de`](https://www.qsmd.de/) and [`gemini.qsmd.de`](gemini://gemini.qsmd.de/).

## Build

Prerequisites: [`git`](https://git-scm.com/), `curl`, [`jq`](https://stedolan.github.io/jq/), `tar`, `make`, `patch`, [`imagemagick`](https://imagemagick.org/), `sshpass` and `sftp` (see also [.gitlab-ci.yml](.gitlab-ci.yml)).

```sh
git clone --recurse-submodules https://framagit.org/qsmd/blog
cd blog/
make VARIANT=www ENV=production build
# now copy the folder public/ to a WWW server
make veryclean
make VARIANT=gemini ENV=production build
# now copy the folder public/ to a Gemini server or use flounder.online:
# make VARIANT=gemini ENV=production FLOUNDER_USERNAME=user FLOUNDER_PASSWORD=pass deploy
```

## Developer information

Some additional information is available in the file [CONTRIBUTING.md](CONTRIBUTING.md).

## License

Except where otherwise noted, content (everything under `content/`) is licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by/4.0/) (or later) and source code (everything else) is licensed under [GNU AGPLv3](http://www.gnu.org/licenses/agpl.html) (or later).

