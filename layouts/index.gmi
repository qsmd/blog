# qsmd

{{ .RawContent | markdownify }}

## {{ i18n "recent_posts" }}

{{- range first 3 (where .Site.Pages "Section" "blog") }}
{{- if eq .Kind "page" }}
### {{ .Title | markdownify }}

{{- if .Summary }}
> {{ .Summary }}
{{- end }}

=> {{ replace .Permalink "index.gmi" "" 1 }} {{ i18n "view_post" }}
{{- end }}
{{- end }}

{{ range .Site.Menus.main }}
{{- if not (or (strings.HasSuffix .URL "suche/") (strings.HasSuffix .URL "search/")) }}
=> {{ replace (.URL | absLangURL) "index.gmi" "" 1 }} {{ .Title | markdownify }}
{{- end }}
{{- end }}
=> ../ ↑ {{ i18n "choose_language" }}
