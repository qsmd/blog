# output variant depending on use case
# either www or gemini
VARIANT ?= www

ifeq ($(VARIANT), www)
VARIANT_WWW = true
endif

ifeq ($(VARIANT), gemini)
VARIANT_GEMINI = true
endif

# output environment depending on build
# either production or development
ENV ?= production

ifeq ($(ENV), production)
ENV_PROD = true
endif

ifeq ($(ENV), development)
ENV_DEV = true
endif

# the destination directory
BUILD_DIR ?= public

# the directory where existing assets used in this Makefile are stored
# calling this "assets" would clash with Hugo's assets folder
MAKE_ASSETS_DIR ?= make-assets

# the directory that needs to be added to the user's PATH
MAKE_BIN_DIR ?= make-bin

# flounder.online credentials
FLOUNDER_USERNAME ?= 
FLOUNDER_PASSWORD ?= 

.PHONY: build converter hugo-assets deploy clean veryclean
.PRECIOUS: $(MAKE_BIN_DIR)/. $(MAKE_ASSETS_DIR)/. ~/.local/share%/. layouts/. layouts%/. assets/. assets%/.

build: converter hugo-assets
ifdef ENV_PROD
  ifdef VARIANT_WWW
	PATH=$(MAKE_BIN_DIR):$$PATH hugo --environment production --gc --verbose --minify --config config.toml,config_WWW.toml --destination $(BUILD_DIR) --cleanDestinationDir
  endif
  ifdef VARIANT_GEMINI
	PATH=$(MAKE_BIN_DIR):$$PATH hugo --environment production --gc --verbose --minify --config config.toml,config_GEMINI.toml --destination $(BUILD_DIR) --cleanDestinationDir
	rm -f $(BUILD_DIR)/index.html # remove DefaultLanguageSubdirRedir
  endif
endif
ifdef ENV_DEV
  ifdef VARIANT_WWW
	PATH=$(MAKE_BIN_DIR):$$PATH hugo --environment development --buildDrafts --buildExpired --buildFuture --gc --verbose --config config.toml,config_WWW.toml --destination $(BUILD_DIR) --cleanDestinationDir
  endif
  ifdef VARIANT_GEMINI
	PATH=$(MAKE_BIN_DIR):$$PATH hugo --environment development --buildDrafts --buildExpired --buildFuture --gc --verbose --config config.toml,config_GEMINI.toml --destination $(BUILD_DIR) --cleanDestinationDir
	rm -f $(BUILD_DIR)/index.html # remove DefaultLanguageSubdirRedir
  endif
endif

converter: $(MAKE_BIN_DIR)/pandoc $(if $(VARIANT_WWW), $(MAKE_BIN_DIR)/pandoc-default ~/.local/share/pandoc/filters/img-lazy-loading.lua ~/.local/share/pandoc/filters/img-add-size.lua ~/.local/share/pandoc/filters/link-rel.lua) $(if $(VARIANT_GEMINI), $(MAKE_BIN_DIR)/md2gmn)

hugo-assets: static/ layouts/robots.txt $(if $(VARIANT_WWW), layouts/partials/header.html layouts/partials/footer.html layouts/partials/pagedjs-header.html static/admin/reset.css assets/css/extended/highlighting.css)

deploy: $(BUILD_DIR)/ $(MAKE_ASSETS_DIR)/deploy_GEMINI.sh
ifdef ENV_PROD
  ifdef VARIANT_GEMINI
	chmod +x $(MAKE_ASSETS_DIR)/deploy_GEMINI.sh
	BUILD_DIR=$(BUILD_DIR) FLOUNDER_USERNAME=$(FLOUNDER_USERNAME) FLOUNDER_PASSWORD=$(FLOUNDER_PASSWORD) DRY_RUN=$(DRY_RUN) $(MAKE_ASSETS_DIR)/deploy_GEMINI.sh
  endif
endif

clean:
	rm -f $(MAKE_BIN_DIR)/pandoc $(MAKE_BIN_DIR)/pandoc-default $(MAKE_BIN_DIR)/md2gmn
	[ "$$(ls -A $(MAKE_BIN_DIR)/)" ] && echo "not empty" || rm -rf $(MAKE_BIN_DIR)/
	rm -f ~/.local/share/pandoc/filters/img-lazy-loading.lua
	rm -f ~/.local/share/pandoc/filters/img-add-size.lua
	rm -f ~/.local/share/pandoc/filters/link-rel.lua
	[ "$$(ls -A ~/.local/share/pandoc/filters/)" ] && echo "not empty" || rm -rf ~/.local/share/pandoc/filters/
	[ "$$(ls -A ~/.local/share/pandoc/)" ] && echo "not empty" || rm -rf ~/.local/share/pandoc/
	rm -f layouts/robots.txt ./layouts/partials/header.html ./layouts/partials/footer.html ./layouts/partials/pagedjs-header.html
	[ "$$(ls -A ./layouts/partials/)" ] && echo "not empty" || rm -rf ./layouts/partials/
	[ "$$(ls -A ./layouts/)" ] && echo "not empty" || rm -rf ./layouts/
	rm -rf static/
	rm -f assets/css/extended/highlighting.css
	[ "$$(ls -A ./assets/css/extended/)" ] && echo "not empty" || rm -rf ./assets/css/extended/
	[ "$$(ls -A ./assets/css/)" ] && echo "not empty" || rm -rf ./assets/css/
	[ "$$(ls -A ./assets/)" ] && echo "not empty" || rm -rf ./assets/
	rm -rf ./resources/_gen/
	[ "$$(ls -A ./resources/)" ] && echo "not empty" || rm -rf ./resources/
	rm -rf ./tmp/

veryclean: clean
	rm -rf ./$(BUILD_DIR)/
	rm -rf ./$(BUILD_DIR)_old/

# for automatic directory creation
# see https://ismail.badawi.io/blog/2017/03/28/automatic-directory-creation-in-make/
$(MAKE_BIN_DIR)/.:
	mkdir -p $@

$(MAKE_ASSETS_DIR)/.:
	mkdir -p $@

~/.local/share%/.:
	mkdir -p $@

layouts/.:
	mkdir -p $@

layouts%/.:
	mkdir -p $@

assets/.:
	mkdir -p $@

assets%/.:
	mkdir -p $@

.SECONDEXPANSION:

$(MAKE_BIN_DIR)/pandoc: $(if $(VARIANT_WWW), $(MAKE_ASSETS_DIR)/pandoc_WWW) $(if $(VARIANT_GEMINI), $(MAKE_ASSETS_DIR)/pandoc_GEMINI) | $$(@D)/.
	cp $< $@
	chmod +x $@

$(MAKE_BIN_DIR)/pandoc-default: | $$(@D)/.
	curl -sL https://api.github.com/repos/jgm/pandoc/releases/latest | jq -r ".assets[].browser_download_url | select (. | contains(\"linux-amd64.tar.gz\"))" | xargs -n 1 curl -sSL | tar -xz --strip-components=2 --wildcards --no-anchored "pandoc" --to-stdout > $@
	chmod +x $@

$(MAKE_BIN_DIR)/md2gmn: | $$(@D)/.
	cd $(MAKE_BIN_DIR)/ && curl -sSL https://git.tdem.in/attachments/7785bc4c-ef23-454f-9569-46fccaaa54df | tar -xz --wildcards "md2gmn"
	chmod +x $@

static/: $(if $(VARIANT_WWW), $(MAKE_ASSETS_DIR)/static_WWW/) $(if $(VARIANT_GEMINI), $(MAKE_ASSETS_DIR)/static_GEMINI/)
	cp -r $< $@

~/.local/share/pandoc/filters/img-lazy-loading.lua: $(MAKE_ASSETS_DIR)/img-lazy-loading.lua | $$(@D)/.
	cp $< $@

~/.local/share/pandoc/filters/img-add-size.lua: $(MAKE_ASSETS_DIR)/img-add-size.lua | $$(@D)/.
	cp $< $@

~/.local/share/pandoc/filters/link-rel.lua: $(MAKE_ASSETS_DIR)/link-rel.lua | $$(@D)/.
	cp $< $@

layouts/robots.txt: $(if $(VARIANT_WWW), $(MAKE_ASSETS_DIR)/robots_WWW.txt) $(if $(VARIANT_GEMINI), $(MAKE_ASSETS_DIR)/robots_GEMINI.txt) | $$(@D)/.
	cp $< $@

layouts/partials/header.html: $(MAKE_ASSETS_DIR)/header.patch | $$(@D)/.
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ ./themes/PaperMod/layouts/partials/header.html $<

layouts/partials/footer.html: $(MAKE_ASSETS_DIR)/footer.patch | $$(@D)/.
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ ./themes/PaperMod/layouts/partials/footer.html $<

layouts/partials/pagedjs-header.html: $(MAKE_ASSETS_DIR)/pagedjs-header.patch | $$(@D)/.
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ ./themes/pagedjs/layouts/partials/pagedjs-header.html $<

static/admin/reset.css: themes/PaperMod/assets/css/reset.css static/
	cp $< $@

assets/css/extended/highlighting.css: $(MAKE_BIN_DIR)/pandoc-default | $$(@D)/.
	template_file=$$(mktemp) && \
	echo '$$highlighting-css$$' > $$template_file && \
	input_file=$$(mktemp) && \
	echo '~~~html' > $$input_file && \
	echo '~~~' >> $$input_file && \
	$(MAKE_BIN_DIR)/pandoc-default --from=markdown --to=html5 --highlight-style=zenburn --template=$$template_file --output=$@ $$input_file && \
	rm $$template_file && \
	rm $$input_file

