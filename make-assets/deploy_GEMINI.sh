#!/bin/bash
# Check for deleted, modified and created files
# and only make the necessary SFTP transactions.

BUILD_DIR_OLD=${BUILD_DIR}_old

# ------------------------
# download current fileset
# ------------------------
if [ ! -d ${BUILD_DIR_OLD} ]; then
mkdir ${BUILD_DIR_OLD}
sshpass -p "${FLOUNDER_PASSWORD}" sftp -P 2024 -o StrictHostKeyChecking=no ${FLOUNDER_USERNAME}@flounder.online <<EOF
get -R * ${BUILD_DIR_OLD}/
exit
EOF
else
OLD_BUILD_EXISTS=true
fi

# -----------------------------------
# calculate difference to new fileset
# -----------------------------------
mkdir tmp/
diff -Naur ${BUILD_DIR_OLD}/ ${BUILD_DIR}/ | lsdiff -s > tmp/diff.txt
cat tmp/diff.txt | grep "^-" | awk '{print $2}' | grep -v "^\.git/*" | sed -e "s-^${BUILD_DIR}/-${BUILD_DIR_OLD}/-" | tee tmp/oldfiles.txt | sed -e "s-^${BUILD_DIR_OLD}/--" | awk '{print "rm " $0}' > tmp/commands_delete_files.txt
cat tmp/diff.txt | grep "^!" | awk '{print $2}' | grep -v "^\.git/*" | sed -e "s-^${BUILD_DIR}/--" | awk '{print "rm " $0}' >> tmp/commands_delete_files.txt
cat tmp/diff.txt | grep "^!" | awk '{print $2}' | grep -v "^\.git/*" | sed -e "s-^${BUILD_DIR}/--" | awk -v BUILD_DIR="${BUILD_DIR}" '{print "put " BUILD_DIR "/" $0 " " $0}' > tmp/commands_create_files.txt
cat tmp/diff.txt | grep "^+" | awk '{print $2}' | grep -v "^\.git/*" | sed -e "s-^${BUILD_DIR}/-${BUILD_DIR_OLD}/-" | tee tmp/newfiles.txt | sed -e "s-^${BUILD_DIR_OLD}/--" | awk -v BUILD_DIR="${BUILD_DIR}" '{print "put " BUILD_DIR "/" $0 " " $0}' >> tmp/commands_create_files.txt

# ------------------------
# identify new directories
# ------------------------

if [ -s tmp/newfiles.txt ]; then
# get directories where new files are located
cat tmp/newfiles.txt | xargs dirname > tmp/newfiles_directories.txt
# for those: also list parent directories
# see https://stackoverflow.com/questions/48336650/how-do-i-print-all-the-parent-directories-in-bash/48336943#48336943
while IFS='/' read -a path
do
    prefix=""
    for ((i=0; i < ${#path[@]}; i++))
    do
      	echo "$prefix${path[i]}"
        prefix="$prefix${path[i]}/"
    done
done < tmp/newfiles_directories.txt | grep -v "^${BUILD_DIR_OLD}$" > tmp/newfiles_directories_parents.txt
# of those: get existing directories
cat tmp/newfiles_directories_parents.txt | xargs -I{} find ${BUILD_DIR_OLD}/ -type d -path {} > tmp/newfiles_directories_parents_existing.txt
# now get not existing directories
grep -v -f <(cat tmp/newfiles_directories_parents_existing.txt | awk '{print "^" $0 "$"}') tmp/newfiles_directories_parents.txt > tmp/newfiles_directories_parents_notexisting.txt
cat tmp/newfiles_directories_parents_notexisting.txt | sed -e "s-^${BUILD_DIR_OLD}/--" | awk '{print "mkdir " $0}' > tmp/commands_create_directories.txt
# simulate directory creation locally
if [ -s tmp/newfiles_directories_parents_notexisting.txt ]; then
cat tmp/newfiles_directories_parents_notexisting.txt | xargs -d'\n' mkdir
fi
# simulate file creation locally
cat tmp/newfiles.txt | xargs -d'\n' touch
else touch tmp/commands_create_directories.txt
fi

# --------------------------
# identify empty directories
# --------------------------

if [ -s tmp/oldfiles.txt ]; then
# simulate file deletion locally
cat tmp/oldfiles.txt | xargs -d'\n' rm -f
# get empty directories
find ${BUILD_DIR_OLD}/ -type d -empty -print | sed -e "s-^${BUILD_DIR_OLD}/--" | awk '{print "rm " $0}' > tmp/commands_delete_directories.txt # using rm instead of rmdir because the latter is not supported at flounder.online
else
touch tmp/commands_delete_directories.txt
fi

cat tmp/commands_delete_files.txt tmp/commands_delete_directories.txt tmp/commands_create_directories.txt tmp/commands_create_files.txt > tmp/commands.txt
echo "exit" >> tmp/commands.txt

if [ ! ${DRY_RUN} = true ]; then
sshpass -p "${FLOUNDER_PASSWORD}" sftp -P 2024 -o StrictHostKeyChecking=no ${FLOUNDER_USERNAME}@flounder.online < tmp/commands.txt
rm -rf tmp/
fi

rm -rf ${BUILD_DIR_OLD}/
if [ "${OLD_BUILD_EXISTS}" = true ]; then
mv ${BUILD_DIR}/ ${BUILD_DIR_OLD}/
fi

