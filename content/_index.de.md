---
title: QSMD
cascade:
  - _target:
      kind: "page"
      path: "/*"
    showtoc: false
---

Commoner. Student. · Für eine sozial-ökologische Transformation, freie Software, Open Science, IT-Sicherheit.
