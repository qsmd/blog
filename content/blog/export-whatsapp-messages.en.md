---
title: Exporting WhatsApp messages from Android
slug: export-whatsapp-messages
date: 2021-01-12T23:21:55.472Z
updated: 2021-01-12T23:21:55.545Z
showtoc: false
summary: With WhatsApp changing its privacy policy in early 2021, accessing previous messages won't be possible without accepting it first. This post covers how to export WhatsApp messages from an Android device on a GNU/Linux system.
---
With WhatsApp changing its privacy policy in early 2021, accessing previous messages won't be possible without accepting it first. This post covers how to export WhatsApp messages from an Android device on a GNU/Linux system.

WhatsApp 2.11.431 was the last version with `adb backup` activated^[_How to manually transfer entire Whatsapp installation to another device (the hard way)_. (2018, January 9). Android Enthusiasts Stack Exchange. Retrieved January 12, 2021, from <https://android.stackexchange.com/questions/150071/how-to-manually-transfer-entire-whatsapp-installation-to-another-device-the-har/189298#189298>]. A downgrade to that version is necessary to export WhatsApp data.

Prerequisites:

- Android Debug Bridge (`adb`)
- Python

1. Install WhatsApp 2.11.431

```sh
# download WhatsApp.apk
wget -O ./WhatsApp-2.11.431.apk https://web.archive.org/web/20191020195549id_/https://www.cdn.whatsapp.net/android/2.11.431/WhatsApp.apk

# verify checksum
sha256sum ./WhatsApp-2.11.431.apk
# expect: 51a387f08debd7e53bcaf9f3174760930787bc5c1dcefb3c8d7ee52b524cd27d

# now connect Android device over USB

# uninstall old version
adb shell pm uninstall -k com.whatsapp

# install new version
adb install ./WhatsApp-2.11.431.apk
```

2. Backup and convert data

```sh
# create and copy backup
adb backup -f ./wa-backup.ab -noapk com.whatsapp

# now unlock your device and confirm the backup operation

# uncompress backup
mkdir ./wa-backup/
( printf "\x1f\x8b\x08\x00\x00\x00\x00\x00" ; tail -c +25 ./wa-backup.ab ) |  tar xfvz - -C ./wa-backup/

# download converter
git clone https://github.com/fingersonfire/Whatsapp-Xtract

# copy images from phone
adb pull /sdcard/WhatsApp/Media ./Whatsapp-Xtract

# convert
python ./Whatsapp-Xtract/whatsapp_xtract.py ./wa-backup/apps/com.whatsapp/db/msgstore.db -w ./wa-backup/apps/com.whatsapp/db/wa.db
cp ./wa-backup/apps/com.whatsapp/db/msgstore.db.html ./Whatsapp-Xtract/
```

The uncompress command is from user Kari on android.stackexchange.com^[_How do you extract an App’s data from a full backup made through “adb backup”?_ (2014, July 27). Android Enthusiasts Stack Exchange. Retrieved January 13, 2021, from <https://android.stackexchange.com/questions/28481/how-do-you-extract-an-apps-data-from-a-full-backup-made-through-adb-backup/78183#78183>].

Additional resources:

 - [WhatsApp Key/DB Extractor](https://github.com/EliteAndroidApps/WhatsApp-Key-DB-Extractor)
 - [WhatsApp Crypt12 Database Decrypter](https://github.com/EliteAndroidApps/WhatsApp-Crypt12-Decrypter)
 - [WhatsApp Key DB Extractor](https://github.com/davidfraser/WhatsApp-Key-DB-Extractor)
