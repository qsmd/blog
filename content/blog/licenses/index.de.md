---
title: Lizenzen – eine Einführung
date: 2021-01-22T11:30:03+00:00
version: 1.1.2
lastmod: 2021-01-30T01:21:40.643Z
slug: lizenzen
summary: Grundlegendes, Empfehlungen, Anwendung
---

# Grundlegendes

## Warum Lizenzen?

Lizenzen sind deswegen von Bedeutung, weil nicht-materielle Erzeugnisse des Menschen (Software, Daten, Texte, Bilder, Musik, Videos etc.; i.A. Träger von Wissen, Kunst und Unterhaltung) in den meisten Ländern nicht ohne Weiteres von allen gleichermaßen verwendet werden dürfen, d.h. es gibt ein "geistiges Eigentum".  In der Regel werden die Autor:innen eines Werks systematisch bevorzugt:  Das betrifft z.B. Veränderung, Verbreitung und öffentliche Wiedergabe.  Dieses rechtliche Ungleichgewicht kann durch Lizenzen etwas abgemildert werden -- aber z.B. in Deutschland nicht vollständig aufgehoben.

## Was sind Lizenzen?

Eine Lizenz regelt die Verwendung eines Werks, indem sie den Empfänger:innen eines Werks bestimmte Rechte einräumt.  Sie schränken hingegen nicht die Rechte von Autor:innen ein, die mit einem Werk auch Dinge machen dürfen, die nach der gewählten Lizenz verboten sind.  In der Praxis handelt es sich bei einer Lizenz um einen (meist englischen) Text, der erläutert, was man darf und was nicht.  Da das Umsetzen einer Absicht in einen juristischen Text nicht ohne Tücken ist, sollte man im Allgemeinen auf vorformulierte Lizenztexte zurückgreifen.  Zu diesen gibt es u.U. bereits Fachmeinungen und Gerichtsurteile, ihre Wirkung ist also besser abschätzbar.  Manche werden zudem von NGOs weiterentwickelt, die den Lizenzen "juristische Rückendeckung" geben.

## Was sind Lizenzen nicht?

Die Verwendung von Lizenzen ist nicht ideal, weil die oben beschriebenen Hürden dadurch nicht abgeschafft werden.  Insbesondere drückt die Verwendung von Lizenzen keinen Widerspruch zur bestehenden Rechtslage aus und trägt sie damit ein Stück weit.  Lizenzen sind also ein Mittel zum Zweck, keine Lösung des dahinterliegenden Problems.

Die hier diskutierten Lizenzen sind zudem nicht oder nur bedingt dazu geeignet, andere rechtliche Hürden bei der Verbreitung von Wissen zu überwinden.  Dazu gehören Patent-, Marken-, Persönlichkeits- und Datenschutzrechte.  Ein Text kann beispielsweise unter einer freien Lizenz stehen und trotzdem einen patentierten Mechanismus beschreiben, für dessen Verwendung Lizenzgebühren gezahlt werden müssen, wenn er eingesetzt wird.

## Worin unterscheiden sich Lizenzen?

### Eingeräumte Rechte

Offensichtlich können sich Lizenzen darin unterscheiden, welche Möglichkeiten der Verwendung erlaubt werden.  Für den Zweck des Commoning soll möglichst viel erlaubt sein.  Einer [verbreiteten Definition](https://freedomdefined.org/Definition/De) folgend gehört dazu:

 - die __Freiheit, ein Werk anzuwenden__ und sich an den genutzten Vorteilen zu erfreuen
 - die __Freiheit, ein Werk zu studieren__ und das dadurch erlangte Wissen anzuwenden
 - die __Freiheit, Kopien des Werkes anzufertigen und zu verbreiten__, ob als Ganzes oder in Teilen des Informationsinhaltes oder der Idee
 - die __Freiheit, Änderungen oder Verbesserungen vorzunehmen__ und abgeleitete Werke weiter zu verteilen

### Einschränkung der Rechte

Gleichzeitig kann eine Lizenz Auflagen machen, unter welchen Voraussetzungen die Rechte eingeräumt werden und sie damit einschränken.  [Beispielhaft](https://freedomdefined.org/Permissible_restrictions/De) dafür sind:

 - Nennung von Autor:innen (sog. _Attribution_)
 - Übertragung von Freiheiten (sog. _Copyleft_)
 - Schutz von Freiheiten (z.B. Erhältlichkeit des Quellmaterials, keine technischen Beschränkungen)

Die folgende Grafik erläutert die Übertragung von Freiheiten, das Copyleft:

![Schematic representation of license directionality[^directionality]](pcbi.1002598.g002.PNG_L.png)

[^directionality]: Darstellung aus [Morin, Urban & Sliz (2012)](https://doi.org/10.1371/journal.pcbi.1002598), lizenziert unter [CC BY 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.de)

Ein tolerant lizenziertes Werk (_permissive_) kann von Empfänger:innen wieder eingehegt werden (_proprietary_, unterste Zeile).  Ein Werk unter Copyleft-Lizenz kann hingegen nicht mehr -- bzw. nur noch von Autor:innen -- eingehegt werden (mittlere Zeile):  Wenn Veränderungen eines Werks weiterverteilt werden, muss dies unter der gleichen Lizenz wie der ursprünglichen geschehen.  Zuweilen wird auch der Begriff _Share-Alike_ statt _Copyleft_ verwendet.

Oft wird auch noch unterschieden zwischen schwachem und starkem Copyleft:  Hier geht es um die Frage, welche Lizenz bei der Veröffentlichung eines größeren Werks gewählt werden kann, das ein Copyleft-Werk zwar nicht verändert, aber darauf aufbaut.  Dies ist z.B. bei sog. Software-Bibliotheken (_libraries_) fraglich, die Funktionalität für andere Programme bereitstellen, oder bei Sammelwerken, die andere Werke beinhalten.  Starkes Copyleft (in der Grafik: _GPL_) fordert auch hier die Verwendung der gleichen Lizenz, während schwaches Copyleft (in der Grafik: _LGPL_) auch andere Lizenzen zulässt.

Es soll noch das Konzept der _Trigger_ erläutert werden:  Ein Trigger beschreibt, wann eine bestimmte Rechte-Einschränkung auch tatsächlich greift.  [Beispielsweise](https://wiki.creativecommons.org/wiki/ShareAlike_compatibility_analysis:_GPL) greifen Attributionsklauseln meist erst, wenn ein Werk auch geteilt wird.  Wird ein Werk nicht geteilt, müssen die Autor:innen auch nicht gekennzeichnet werden.  [Ganz ähnlich](https://wiki.creativecommons.org/wiki/ShareAlike_compatibility_analysis:_GPL) bei (typischen) Copyleft-Klauseln:  Diese greifen erst, wenn ein Werk, das verändert wurde, auch geteilt wird.  Autor:innen der veränderten Werke müssen dann die Auflagen der Lizenz erfüllen, die Werke also wieder unter die gleiche Lizenz stellen und ggf. so etwas wie einen Quelltext zur Verfügung stellen.  Wird ein Werk, das unter einer Copyleft-Lizenz steht, lediglich verändert, aber nicht mit anderen geteilt, verpflichtet das meist _nicht_ dazu, die Auflagen zu erfüllen.  Und eine Pflicht zum Weiterverteilen gehört nicht zu Copyleft-Lizenzen.

Das führt direkt zu einem verwandten Punkt:  Copyleft bezieht sich auf einen Rechteerhalt "flussabwärts" (engl. _downstream_), also zu den Empfänger:innen.  Diese sollen die gleichen Rechte bekommen wie die Person, die das Werk gerade weiterverteilt.  Die Rechte derer, die keine Empfänger:innen sind, werden auch nicht geschützt, insb. also auch nicht die von ursprünglichen Autor:innen, wenn sie nicht auch zugleich Empfänger:innen sind.  Ursprüngliche (engl. _upstream_) Autor:innen eines Programms, das unter einer Copyleft-Lizenz für Software steht, haben also nicht automatisch ein Recht darauf, den Quelltext jeder sich im Umlauf befindlichen Veränderung ihres Werks zu erhalten -- erst, wenn sie selbst wieder eine Kopie ihres veränderten Werks erhalten.  In der Praxis spielt dieser Punkt freilich eine eher untergeordnete Rolle, sofern das Werk nicht manuell ausgehändigt wird oder die Verteilung aus anderen Gründen langsam erfolgt.

### Weitere Unterschiede

Darüber hinaus unterscheiden sich Lizenzen noch hinsichtlich folgender Kriterien:

 - __Werktyp__: Nicht jede Lizenz ist für jede Art von Werk geeignet, es gibt spezielle Lizenzen für Software, Texte, Kunstwerke, Schriftarten, Datenbanken usw.
 - __Rechtsraum__: Manche Lizenzen sind für einen bestimmten Rechtsraum geschrieben.
 - __Kompatibilität__: Manche Lizenzen definieren andere Lizenzen als "kompatibel".
 - __Positionierung zum Urheberrecht/Copyright__: Manche Lizenzen machen explizit, dass die Lizenzgeber:innen Urheberrecht bzw. Copyright ablehnen.

## Welche Kontroversen gibt es im Zusammenhang mit Lizenzen?

### Einsatzzweck

Teilweise umstritten ist, ob Lizenzen dafür genutzt werden sollten, Probleme anderer Domänen zu lösen: Kommerzialisierung, Militarisierung, Menschenrechtsverletzung, Klimawandel etc. (siehe [Ethical Licenses](https://ethicalsource.dev/licenses/)).  Beispielsweise gibt es Lizenzen, die nur die nicht kommerzielle Nutzung eines Werks erlauben.  Einige dem Commoning wohlgesonnene Akteur:innen raten allerdings zur Vorsicht:

 - Wikimedia Foundation: [Freies Wissen dank Creative-Commons-Lizenzen](https://meta.wikimedia.org/wiki/Free_knowledge_based_on_Creative_Commons_licenses/de)
 - Free Software Foundation: [Why programs must not limit the freedom to run them](https://www.gnu.org/philosophy/programs-must-not-limit-freedom-to-run.html)
 - Creative Commons: [verweist](https://creativecommons.org/share-your-work/public-domain/freeworks/) auf die Definition von [Free Cultural Works](https://freedomdefined.org/Licenses/NC/De)

### Lizenzierung von Beiträgen

Lizenzen betreffen die Empfänger:innen eines Werks, schränken aber nicht die Rechte von Autor:innen ein.  Sobald nun neue Menschen zu einem Werk beitragen, werden die ursprünglichen Autor:innen zu Empfänger:innen der neuen Beiträge.  Um die Frage, welche Rechte die ursprünglichen Autor:innen bekommen, geht es bei der _Lizenzierung von Beiträgen_.

Bei gemeinschaftlich entwickelten Werken ist manchmal die Rede von ausgehenden ("outbound") und eingehenden ("inbound") Lizenzen.  Ausgehende Lizenzen beziehen sich auf das bestehende Werk und geben Nutzer:innen bestimmte Rechte.  Eingehende Lizenzen beziehen sich auf neue Beiträge zum Werk und geben den ursprünglichen Autor:innen bestimmte Rechte.  In vielen Freie-Software-Projekten gilt "inbound = outbound", damit bekommen Autor:innen die gleichen Rechte wie Empfänger:innen.  Manchmal ist das aber auch anders, etwa wenn sich die ursprünglichen Autor:innen die Möglichkeit vorbehalten möchten, als einzige mit dem Werk Geld zu verdienen, das Werk parallel unter einer anderen Lizenz zu vertreiben oder die Lizenz auch später noch ändern zu können.  Dies wird dann mit speziellen _Contributor License Agreements_ (CLA) geregelt, die allerdings auch [kritisiert werden](https://drewdevault.com/2018/10/05/Dont-sign-a-CLA.html).  Es besteht nämlich die Gefahr, dass ein Werk durch eine nachträgliche Lizenzänderung von den Autor:innen wieder eingehegt wird.  Da für so eine Entscheidung die Zustimmung aller Autor:innen benötigt wird, sind Werke umso resistenter gegen Einhegungen, je größer der Autor:innenpool ist.

### Begrifflichkeiten

Zuletzt wird auch um die richtige Bezeichnung von "guten" Lizenzen bzw. lizenzierten Werken gerungen.  Folgende Begriffe stehen zur Debatte:

|  |idealistisch|pragmatisch|
|--|--|--|
|Software|[Freie Software](https://www.gnu.org/philosophy/free-sw.de.html)|[Open Source](https://opensource.org/docs/osd)|
|Weiteres|[Freie kulturelle Werke](https://freedomdefined.org/Definition/De)|[Open](https://opendefinition.org/)|

Table: Tabelle: Ausrichtung der Konzepte[^ausrichtung]

[^ausrichtung]: Darstellung nach [Schnalke (2014)](https://doi.org/10.11588/pb.2014.2.16806), lizenziert unter [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)

Ein Werk fällt genau dann unter einen der vier Begriffe, wenn es unter einer passenden Lizenz veröffentlicht wurde.  Für alle Begriffe gibt es je eine Institution, die nach einer Definition entscheidet, welche Lizenzen "passen".  Für Software sind dies die _Free Software Foundation_ (FSF) sowie die _Open Source Initiative_ (OSI).

Die FSF nennt freie Software solche, die Nutzer:innen die Freiheiten einräumt, die Software zu verwenden, zu verstehen, zu verbreiten und zu verbessern.  Sie beschreibt _nicht-freie_ (proprietäre) Software als gesellschaftliches Problem, weil sie Teilen und Kooperation verhindert und möchte durch ihre Lizenzen das Machtgefälle zwischen Entwickler:innen und Nutzer:innen ausgleichen.  Diesem ethisch aufgeladenen Begriff von _freier Software_ wurde nach einer Weile der Begriff _Open Source_ entgegengesetzt, der vor allem die Effizienzsteigerung betont, die sich durch das Teilen des Quelltextes ergibt.  Die Bezeichnung _Freies Kulturelles Werk_ und _Open_ sind in Anlehnung daran für alle weiteren Werkarten entstanden.

Trotz etwas unterschiedlicher Definitionen ist _freie_ Software meist auch _open source_ und umgekehrt.  Es geht bei der Unterscheidung also weniger um unterschiedliche Anforderungen an Software, sondern mehr um eine andere Motivation.  Beide Begriffe sind zudem etwas missverständlich, da _frei_ etwas anderes meint als _gratis_, und _open source_ mehr als Verfügbarkeit des Quelltextes ("source available").  Manchmal ist im Englischen daher statt _free software_ auch von _libre software_ die Rede, um eine Verwechslung mit kostenloser Software auszuschließen.  Weil Begrifflichkeiten in der Vergangenheit zu einigen Verwerfungen führten, benutzen manche auch die Bezeichnung _FLOSS_ als Abkürzung für _free/libre open source software_.

# Empfehlungen

Wenn Autor:innen eines Werks den Empfänger:innen möglichst umfassende Freiheit zu dessen Verwendung geben möchten, bleibt eine Frage:  Sollen die Empfänger:innen die Freiheit bekommen, anderen die Freiheit wieder zu nehmen?  Konkret: Darf Wissen, wenn es einmal freigesetzt wurde, wieder eingehegt werden?  Eine sog. _Copyleft_-Lizenz kann dies verhindern, insb. eine starke Copyleft-Lizenz.

Für die Zwecke des Commonings ist zu empfehlen, eine der beiden möglichen Extrempositionen anzunehmen:  Entweder bestehende Rechtsmechanismen maximal nutzen, um zu verhindern, dass Wissen wieder eingehegt wird.  Oder eine Entlassung in die Gemeinfreiheit anstreben, so weit dies nach nationalem Recht möglich ist.  Welchen Weg man geht, kann von dem Umfang des Werks und der Verbreitung ähnlicher Werke abhängig gemacht werden:  Handelt es sich nur um kleine Notizen?  Dann ist die virale Eigenschaft einer Copyleft-Lizenz nicht so bedeutsam und man [kann](https://www.gnu.org/licenses/license-recommendations.html#small) die Gemeinfreiheit wählen.  Oder gibt es mutmaßlich noch keine vergleichbaren Werke, weswegen es möglicherweise von vielen Menschen genutzt werden wird?  Dann kommt dem neuen Werk besondere Bedeutung zu und es [sollte](https://www.gnu.org/licenses/why-not-lgpl.html) entsprechend [verteidigt](https://drewdevault.com/2020/07/27/Anti-AGPL-propaganda.html) werden.

Diese Empfehlungen zur Lizenzwahl betreffen nur neue Werke und nicht die Lizenzen von Beiträgen zu bestehenden Werken.

![Entscheidungsbaum zur Lizenzwahl](entscheidungsbaum.svg)

## Einhegung von Wissen verhindern

Um die Einhegung von Wissen zu verhindern, ist eine Copyleft-Lizenz unumgänglich.  Leider sind Copyleft-Lizenzen untereinander in der Regel inkompatibel, da sie ja gerade fordern, dass Veränderungen eines Werks unter der _je eigenen Lizenz_ weitergegeben werden.  Das ist immer dann ein Problem, wenn ein Werk -- wie bei Software -- stetig verändert wird oder -- wie bei Kunst -- ständig neu zusammengestellt wird.  Daher ist bei der Entscheidung auch zu berücksichtigen, ob eine Copyleft-Lizenz für einen bestimmten Werktyp schon im besonderen Maß verbreitet ist.

### Software (und Werke, für die es eine bevorzugte Form der Bearbeitung gibt)

Für Software empfiehlt sich die Verwendung der _GNU Affero General Public License_ ([AGPL, Version 3 oder neuer](https://www.gnu.org/licenses/agpl-3.0.html)).  Sie eignet sich allerdings nicht nur für Software, sondern für _alle möglichen Werke, für die es eine bevorzugte Form der Bearbeitung gibt_.  Bei Software ist diese bevorzugte Form der Bearbeitung der Quelltext (und nicht die ausführbaren Binärdateien), bei Dokumenten kann es z.B. eine LibreOffice-Datei (und nicht die daraus erzeugte PDF-Datei) sein.

[![AGPLv3](AGPLv3_Logo.svg){width=120px height=48px}](https://www.gnu.org/licenses/agpl-3.0)

Die AGPL hat ihren Ursprung bei der _Free Software Foundation_, die einige juristische Erfahrung sowohl beim Verfassen als auch beim Verteidigen von Lizenzen hat.  Diese Lizenz ist [vereinbar](https://www.gnu.org/licenses/license-list.de.html#AGPL) mit der _GNU General Public License_ ([GPL](https://www.gnu.org/licenses/gpl)), was als notwendige Voraussetzung für Software-Lizenzen [gilt](https://dwheeler.com/essays/gpl-compatible.html).  Von diesen ist sie die momentan stärkste Copyleft-Lizenz, um eine erneute Einhegung zu verhindern, insbesondere was Software betrifft, die über das Internet genutzt wird.

Sie enthält eine Klausel, die Patentansprüche der Autor:innen derart berücksichtigt, dass diese nicht gegen Empfänger:innen des Werks geltend gemacht werden können.  Im Vergleich zur Apache License 2.0, die keine Copyleft-Lizenz ist, enthält sie keinen expliziten Ausschluss von Markenrechten und leider auch keinen expliziten Absatz zur [Lizenz von Beiträgen zum Ursprungswerk](https://apetro.ghost.io/apache-contributors-no-cla/).

[Besonders](https://www.gnu.org/licenses/identify-licenses-clearly.html) bei den Lizenzen der Free Software Foundation ist darauf zu achten, dass man nicht nur die Versionsnummer der Lizenz angibt, sondern auch dazu schreibt, ob das Werk nur unter genau dieser Version oder auch unter einer späteren Version verwendet werden darf.  In diesem Fall ist empfehlenswert, auch alle späteren Versionen der AGPL zu erlauben, um von möglichen Aktualisierungen der Lizenz profitieren zu können.

### Text, Bild, Audio, Video, Daten(-banken)

Für Inhalte im Text-, Bild-, Audio- und Videoformat sowie für Datenbanken empfiehlt sich die Verwendung der Creative Commons-Lizenz _Namensnennung -- Weitergabe unter gleichen Bedingungen 4.0 International_ ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de) oder neuer).

[![CC BY-SA 4.0](by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/deed.de)

_Creative Commons_ ist ebenfalls eine amerikanische NGO, die auch [umfassende Hilfeseiten](https://creativecommons.org/) sowie ein [Wiki](https://wiki.creativecommons.org/) zur Anwendung ihrer Lizenzen bereitstellt.  Es handelt sich bei der gewählten Lizenz um eine _[Free Culture License](https://freedomdefined.org/Licenses)_, außerdem ist sie [einseitig kompatibel](https://creativecommons.org/compatiblelicenses) zur GPLv3, die wiederum kompatibel zur AGPLv3 ist.

Die Lizenz verbietet explizit technische Einschränkungen, die die Verwendung eines Werks wieder einschränken (wie z.B. Kopierschutzmechanismen).  Größte Schwäche ist, dass sie keine Klausel bzgl. der Form, in der ein Werk verteilt wird, enthält.  Deswegen sollte sie [nicht](https://opensource.stackexchange.com/questions/1717/why-is-cc-by-sa-discouraged-for-code/1718#1718) für Software verwendet werden.  Wenn es für ein Werk eine klar bevorzugte Form der Bearbeitung gibt, kann stattdessen die AGPL (siehe oben) verwendet werden.  Die CC BY-SA 4.0 enthält weiterhin leider keine Klausel bzgl. möglicher Patentansprüche der Autor:innen und ist auch "nur" eine [schwache Copyleft-Lizenz](https://wiki.creativecommons.org/wiki/License_Versions#Licensing_of_collections).

Auch Creative Commons [merkt an](https://creativecommons.org/platform/toolkit/#updating-to-later-versions), dass man bereits bei der Veröffentlichung eines Werks dazuschreiben kann, dass es auch unter neueren (zukünftigen) Lizenzversionen genutzt werden darf.  Neuere Lizenzversionen reagieren auf Veränderungen der Rechtslage oder machen Dinge klarer.  Mit [Version 4](https://creativecommons.org/Version4) wurde beispielsweise explizit gemacht, dass ein:e Autor:in andere auch nachträglich noch auffordern kann, die Namensnennung zu entfernen, ebenso wurde durch einen 30-Tage-Wiedergutmachungszeitraum ein [Schutz vor Copyright-Trollen](https://www.heise.de/meinung/Edit-Policy-Copyright-Trolle-gezielter-Missbrauch-von-Creative-Commons-4967790.html) implementiert.

## In die Gemeinfreiheit entlassen

Um ein Werk in die Gemeinfreiheit zu entlassen, ist für jede Art von Werk die Verwendung der [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) empfohlen.

Sie enthält eine Fallback-Lizenz für Rechtsräume, in denen ein Totalverzicht auf das Urheberrecht nicht möglich ist (z.B. Deutschland) und kommt ebenfalls von _Creative Commons_.  Anderen Lizenzen mit ähnlicher Intention (Do What The Fuck You Want To Public License, Unlicense) fehlt dies.  Als Schwäche wird ihr allerdings die explizite Nicht-Gewährung von Patentrechten [ausgelegt](https://opensource.org/faq#cc-zero), während sich manche andere Lizenzen dazu "nur" ausschweigen -- diese Klarheit kann aber auch als Vorteil gesehen werden.

# Anwendung

Ein eigenes Werk unter einer freien Lizenz zu lizenzieren ist leicht, man schreibt es nämlich einfach dazu.  Also z.B.

> Dieses Werk ist lizenziert unter [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de) (oder neuer).
> 
> [![CC BY-SA 4.0](by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0/deed.de)

Dazu kann, wie hier geschehen, eine bildliche Darstellung der Lizenz eingefügt werden.  Dabei nicht vergessen:

 - eine Lizenzversion anzugeben
 - ggf. auch neuere Versionen der Lizenz zu erlauben
 - einen Link zur Lizenzbeschreibung einzufügen, wenn das Medium es zulässt

Eine "Registrierung" bei irgendeiner Stelle ist nicht nötig.  Die Lizenzierung ist rechtlich gesehen unwiderruflich, kann also nicht zurückgenommen werden.  Keine der hier empfohlenen Lizenzen hindert Autor:innen daran, das Werk nur gegen Geld abzugeben.  Wenn sie möchten, können Autor:innen auch auf die Namensnennung verzichten.  Aufpassen müssen sie, wenn das Werk im Rahmen eines Arbeitsvertrages entstanden ist, denn dann haben sie möglicherweise gar nicht das Recht, das Werk unter eine freie Lizenz zu stellen.

Außerdem ist es empfehlenswert, die Lizenzangabe maschinenlesbar zu gestalten.  Das geht für HTML ganz einfach mittels RDFa durch Hinzufügen des Attributs `rel="license"` und erhöht die Reichweite des Werks, indem z.B. Suchmaschinen die Lizenz erkennen können:

```html
Dieses Werk ist lizenziert unter
<a rel="license"
  href="http://creativecommons.org/licenses/by/4.0/"
  >CC&nbsp;BY-SA&nbsp;4.0</a>
(oder neuer).
```

Mittels der _Rights Expression Language_ [lassen sich](https://labs.creativecommons.org/2011/ccrel-guide/#Basic_Marking) auch noch weitere Informationen über das Werk maschinenlesbar hinterlegen:

```html
<p>
    <a
      href="https://creativecommons.org/licenses/by-sa/4.0/deed.de"><img
          alt="Creative Commons"
          style="height:22px!important; margin-left:3px; vertical-align:text-bottom;"
          src="https://mirrors.creativecommons.org/presskit/icons/cc.svg" /><img
          alt="Attribution"
          style="height:22px!important; margin-left:3px; vertical-align:text-bottom;"
          src="https://mirrors.creativecommons.org/presskit/icons/by.svg" /><img
          alt="ShareAlike"
          style="height:22px!important; margin-left:3px; vertical-align:text-bottom;"
          src="https://mirrors.creativecommons.org/presskit/icons/sa.svg" /></a>
</p>
<p
  xmlns:dct="http://purl.org/dc/terms/"
  xmlns:cc="http://creativecommons.org/ns#"
  property="dct:RightsStatement">
    <a
      rel="cc:attributionURL"
      href="http://work.url/"
      property="dct:title">Titel</a>
    von
    <a
      rel="cc:attributionURL dct:creator dct:rightsHolder"
      href="http://author.url/"
      property="cc:attributionName">Autor</a>
    ist lizenziert unter
    <a
      rel="license noreferrer"
      href="http://creativecommons.org/licenses/by-sa/4.0/"
      >CC&nbsp;BY-SA&nbsp;4.0</a>
    (oder neuer).
</p>
```

Für andere Formate wie PDF lässt sich zur Einbettung von Metadaten der ISO-Standard _Extensible Metadata Platform_ verwenden.
