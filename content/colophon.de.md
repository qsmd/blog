---
title: Kolophon
slug: kolophon
menu: main
weight: 3
---

> | Wo kämen wir hin,
> | wenn alle sagten,
> | wo kämen wir hin,
> | und niemand ginge,
> | um einmal zu schauen,
> | wohin man käme,
> | wenn man ginge.
> 
> --- Kurt Marti: Der Traum, geboren zu sein.

Diese Website ist sowohl über das World Wide Web als auch über den [Geminispace](https://gemini.circumlunar.space/) erreichbar:

- WWW: [`https://www.qsmd.de/`](https://www.qsmd.de/)
- Geminispace: [`gemini://gemini.qsmd.de/`](gemini://gemini.qsmd.de/)

Jede Seite ist über beide Protokolle und korrespondierende URLs erreichbar. Das sieht für diese Seite z.B. wie folgt aus:

- WWW: [`https://www.qsmd.de/de/kolophon/`](https://www.qsmd.de/en/colophon/)
- Geminispace: [`gemini://gemini.qsmd.de/de/kolophon/`](gemini://gemini.qsmd.de/en/colophon/)

Die Website wird größtenteils durch [freie Software](https://www.gnu.org/philosophy/free-sw.html) ermöglicht: Ich nutze [Netlify CMS](https://www.netlifycms.org/) für das Verfassen von Texten im Markdown-Format, die mit Hilfe von [GitLab](https://about.gitlab.com/) versioniert auf den Servern von [Framagit](https://framagit.org/) gespeichert werden. Dort wird durch [Hugo](https://gohugo.io/), [Pandoc](https://pandoc.org/) und [md2gmn](https://git.tdem.in/tdemin/gmnhg#md2gmn) eine statische Website erzeugt. Dabei wird das Theme [PaperMod](https://github.com/adityatelange/hugo-PaperMod/) verwendet und [Paged.js](https://gitlab.pagedmedia.org/julientaq/pagedjs-hugo) für die Druckansicht eingebunden. Das Hosting wird ebenfalls von Framagit sowie von [flounder](//flounder.online/) übernommen. Die Icons stammen aus den Sammlungen [Feather](https://feathericons.com/) (MIT/Expat-Lizenz) und [Logobridge](https://logobridge.co/) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)).

Sofern nicht anders gekennzeichnet, steht der Inhalt dieser Website unter der Lizenz [CC BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/deed.de) (oder neuer). Der Quelltext kann unter folgendem Link eingesehen werden: [`framagit.org/qsmd/blog`](https://framagit.org/qsmd/blog). Es werden keine Tracker oder Cookies eingesetzt. Wenn man das Farbschema der Website ändert, wird die Wahl im Browser gespeichert, das lässt sich aber [leicht wieder löschen](https://support.mozilla.org/kb/storage).
