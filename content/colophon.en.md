---
title: Colophon
slug: colophon
menu: main
weight: 3
---
This site is available both on the world wide web and on the [Gemini space](https://gemini.circumlunar.space/):

- WWW: [`https://www.qsmd.de/`](https://www.qsmd.de/)
- Gemini space: [`gemini://gemini.qsmd.de/`](gemini://gemini.qsmd.de/)

Every page is accessible over both protocols with corresponding URLs. For example for this colophon page:

- WWW: [`https://www.qsmd.de/en/colophon/`](https://www.qsmd.de/en/colophon/)
- Gemini space: [`gemini://gemini.qsmd.de/en/colophon/`](gemini://gemini.qsmd.de/en/colophon/)

The site is largely powered by [free/libre software](https://www.gnu.org/philosophy/free-sw.html). [Netlify CMS](https://www.netlifycms.org/) is used for everyday authoring in markdown format. [Hugo](https://gohugo.io/), [Pandoc](https://pandoc.org/) and [md2gmn](https://git.tdem.in/tdemin/gmnhg#md2gmn) are used to create a static website where [PaperMod](https://github.com/adityatelange/hugo-PaperMod/) is used as theme and [Paged.js](https://gitlab.pagedmedia.org/julientaq/pagedjs-hugo) is used for pretty printing. [flounder](//flounder.online/) and [GitLab](https://about.gitlab.com/) running at [Framagit](https://framagit.org/) are used to host the source code and the website. Icons are taken from the [Feather](https://feathericons.com/) (MIT/Expat) and [Logobridge](https://logobridge.co/) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/)) collections.

All content on this site is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/) (or later). The source code can be accessed via the following link: [`framagit.org/qsmd/blog`](https://framagit.org/qsmd/blog). No trackers or cookies are being used. Changing the color scheme will save one's choice in the browser, but this can [easily be deleted again](https://support.mozilla.org/kb/storage).
