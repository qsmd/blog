
# Initialization

```sh
git init
git checkout -b main
# or use git init --initial-branch=main
git config user.email "mail@qsmd.de"
git config user.name "QSMD"
git remote add origin https://framagit.org/qsmd/blog.git
git submodule add https://github.com/adityatelange/hugo-PaperMod.git themes/PaperMod --depth=1
git submodule add https://gitlab.pagedmedia.org/julientaq/pagedjs-hugo.git themes/pagedjs --depth=1
git submodule update --init --recursive
git add .
git commit -m "Initial commit"
git push -u origin main
```

- add GitLab application with scope `api` for Netlify CMS and store the token in a CI/CD variable called `API_PRIVATE_TOKEN`
- store the flounder.online password in a CI/CD variable called `FLOUNDER_PASSWORD`

# Making changes to files under `themes/`

## Changing a previously unchanged file

```sh
cp themes/<THEME>/layouts/<FILEPATH> layouts/<FILEPATH> # create a custom copy
# change your custom copy
diff -Naur themes/<THEME>/layouts/<FILEPATH> layouts/<FILEPATH> > make-assets/<FILENAME>.patch # generate patch file
# add the patch code to the Makefile
make clean # remove generated files
# commit
```

## Changing an already changed file

```sh
make # generate existing file from patch
# now edit existing file
diff -Naur themes/<THEME>/layouts/<FILEPATH> layouts/<FILEPATH> > make-assets/<FILENAME>.patch # generate new patch file
make clean # remove generated files again
# commit
```

# Offene Punkte

# Fixes

## Small changes

- type=search umsetzen
- add PGP key visibly
- add information about the last modification date

## Consider use of Unicode symbols

- see [New font with Unicode-compatible Creative Commons license symbols](https://www.ctrl.blog/entry/creative-commons-unicode-fallback-font.html)

## Move always included pagedjs stylesheet in the folder for additional sheets and remove insertion code for old location

- good idea? will break modularity

# Update

- `git submodule update --remote themes/matomo`
- `git submodule update --init --recursive`

# Inspiration

- [thedivtagguy](https://thedivtagguy.netlify.app/) ([source](https://github.com/thedivtagguy/website2))
- [fedetibaldo](https://fedetibaldo.com/) (CC icons)

